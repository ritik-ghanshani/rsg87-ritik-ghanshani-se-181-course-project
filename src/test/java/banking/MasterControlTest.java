package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MasterControlTest {

    MasterControl masterControl;
    ArrayList<String> input;

    @BeforeEach
    void setup() {
        input = new ArrayList<>();
        Bank bank = new Bank();
        masterControl = new MasterControl(bank, new ValidateInput(bank), new Parser(bank), new CommandStorage());
    }

    private void assertSingleCommand(String command, List<String> actual) {
        assertEquals(1, actual.size());
        assertEquals(command, actual.get(0));
    }

    @Test
    void typo_in_create_command_is_invalid() {
        input.add("creat checking 12345678 1.8");

        List<String> actual = masterControl.start(input);
        assertSingleCommand("creat checking 12345678 1.8", actual);
    }

    @Test
    void typo_in_deposit_command_is_invalid() {
        input.add("depositt 12345678 100");

        List<String> actual = masterControl.start(input);
        assertSingleCommand("depositt 12345678 100", actual);
    }

    @Test
    void two_typo_commands_both_invalid() {
        input.add("creat checking 12345678 1.0");
        input.add("depositt 12345678 100");

        List<String> actual = masterControl.start(input);
        assertEquals(2, actual.size());
        assertEquals("creat checking 12345678 1.0", actual.get(0));
        assertEquals("depositt 12345678 100", actual.get(1));
    }

    @Test
    public void invalid_to_create_accounts_with_same_ID() {
        this.input.add("create checking 12345678 1.0");
        this.input.add("create checking 12345678 1.0");

        List<String> actual = this.masterControl.start(this.input);

        assertEquals(2, actual.size());
    }

    @Test
    void sample_make_sure_this_passes_unchanged_or_you_will_fail() {
        input.add("Create savings 12345678 0.6");
        input.add("Deposit 12345678 700");
        input.add("Deposit 12345678 5000");
        input.add("creAte cHecKing 98765432 0.01");
        input.add("Deposit 98765432 300");
        input.add("Transfer 98765432 12345678 300");
        input.add("Pass 1");
        input.add("Create cd 23456789 1.2 2000");
        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 1000.50 0.60", actual.get(0));
        assertEquals("Deposit 12345678 700", actual.get(1));
        assertEquals("Transfer 98765432 12345678 300", actual.get(2));
        assertEquals("Cd 23456789 2000.00 1.20", actual.get(3));
        assertEquals("Deposit 12345678 5000", actual.get(4));
    }

    @Test
    public void clear_account_history_after_deletion() {
        this.input.add("Create checking 12345678 0.6");
        this.input.add("Deposit 12345678 300");
        this.input.add("Withdraw 12345678 300");
        this.input.add("Pass 1");
        this.input.add("Create savings 12345678 0.6");
        List<String> actual = this.masterControl.start(this.input);
        assertSingleCommand("Savings 12345678 0.00 0.60", actual);
    }

    @Test
    public void zero_commands_return() {
        this.input.add("create checking 12345678 0.01");
        this.input.add("create savings 12345677 0.01");
        this.input.add("deposit 12345678 400");
        this.input.add("transfer 12345678 12345677 400");
        this.input.add("withdraw 12345677 400");
        this.input.add("pass 1");

        List<String> actual = this.masterControl.start(this.input);
        assertEquals(actual.size(), 0);
    }

    @Test
    public void validate_deposit_command() {
        this.input.add("create checking 12345678 0.01");
        this.input.add("Deposit 12345678 400");

        List<String> actual = this.masterControl.start(this.input);
        assertEquals(actual.size(), 2);
    }

    @Test
    public void validate_withdraw_command() {
        this.input.add("create checking 12345678 0.01");
        this.input.add("Deposit 12345678 500");
        this.input.add("Withdraw 12345678 300");

        List<String> actual = this.masterControl.start(this.input);
        assertEquals(actual.size(), 3);
    }

    @Test
    public void validate_transfer_command() {
        this.input.add("create checking 12345678 0.01");
        this.input.add("Deposit 12345678 100");
        this.input.add("create Savings 12345677 0.01");
        this.input.add("Deposit 12345677 400");
        this.input.add("Transfer 12345678 12345677 400");
        List<String> actual = this.masterControl.start(this.input);
        assertEquals(actual.size(), 6);
    }

    @Test
    public void make_multiple_accounts() {
        this.input.add("create SaVings 12345678 0.01");
        this.input.add("deposit 12345678 780");
        this.input.add("create checking 12345677 0.01");
        this.input.add("create CD 12345679 1 6000");
        this.input.add("pass 1");

        List<String> actual = this.masterControl.start(this.input);
        assertEquals(actual.size(), 3);
    }

}
