package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BankTest {

    public static final String ACCT_NUMBER1 = "12345678";
    public static final String ACCT_NUMBER2 = "12345679";
    public static final String ACCT_NUMBER3 = "12345677";
    public static final Double STARTING_BALANCE = 500.00;
    public static final Double APR = 5.3;
    public static Account checking;
    public static Account savings;
    public static Account cd;
    Bank bank;

    @BeforeEach
    void setup() {
        bank = new Bank();
        checking = new Checking(ACCT_NUMBER1, APR);
        savings = new Savings(ACCT_NUMBER2, APR);
        cd = new Cd(ACCT_NUMBER3, APR, STARTING_BALANCE);
    }

    @Test
    void bank_has_no_accounts_initially() {
        Bank bank = new Bank();
        assertTrue(bank.getAccounts().isEmpty());
    }

    @Test
    void add_account_to_bank() {
        Bank bank = new Bank();
        bank.addAccount(checking);
        assertEquals(ACCT_NUMBER1, bank.getAccounts().get(ACCT_NUMBER1).getAccountID());
    }

    @Test
    void add_two_accounts_to_bank() {
        String NEW_ACCT_ID = "12345670";
        bank.addAccount(checking);
        bank.addAccount(new Checking(NEW_ACCT_ID, APR + 1));
        assertEquals(NEW_ACCT_ID, bank.getAccounts().get(NEW_ACCT_ID).getAccountID());
    }

    @Test
    void deposit_into_account() {
        bank.addAccount(checking);
        bank.deposit(ACCT_NUMBER1, 500.00);
        assertEquals(500.00, bank.getAccounts().get(ACCT_NUMBER1).getBalance());
    }

    @Test
    void withdraw() {
        bank.addAccount(checking);
        bank.deposit(ACCT_NUMBER1, 60.00);
        bank.withdraw(ACCT_NUMBER1, 30.00);
        assertEquals(30.00, bank.getAccounts().get(ACCT_NUMBER1).getBalance());
    }

    @Test
    void apr() {
        assertTrue(bank.validateAPR(0.00));
        assertTrue(bank.validateAPR(5.00));
        assertFalse(bank.validateAPR(20.00));
        assertFalse(bank.validateAPR(-1.00));
    }

    @Test
    void id_exists() {
        bank.addAccount(checking);
        assertTrue(bank.idExists("12345678"));
        assertFalse(bank.idExists("12345679"));
    }

    @Test
    void validate_apr() {
        assertFalse(bank.validateAPR(15));
        assertTrue(bank.validateAPR(10));
        assertTrue(bank.validateAPR(0));
        assertTrue(bank.validateAPR(6));
    }

    @Test
    void getAccountState() {
        bank.addAccount(checking);
        bank.addAccount(savings);
        bank.addAccount(cd);
        assertEquals("Checking 12345678 0.00 5.30", bank.getAccountState(ACCT_NUMBER1));
        assertEquals("Savings 12345679 0.00 5.30", bank.getAccountState(ACCT_NUMBER2));
        assertEquals("Cd 12345677 500.00 5.30", bank.getAccountState(ACCT_NUMBER3));
    }

    @Test
    void can_transfer() {
        bank.addAccount(checking);
        bank.addAccount(savings);
        bank.addAccount(cd);
        assertTrue(bank.canTransfer(ACCT_NUMBER1));
        assertTrue(bank.canTransfer(ACCT_NUMBER2));
        assertFalse(bank.canTransfer(ACCT_NUMBER3));
    }

    @Test
    void validate_withdraw_amount() {
        bank.addAccount(checking);
        bank.addAccount(savings);
        bank.addAccount(cd);
        assertTrue(bank.validateWithdrawAmount(ACCT_NUMBER1, 300.00));
        assertTrue(bank.validateWithdrawAmount(ACCT_NUMBER2, 500.00));
        assertFalse(bank.validateWithdrawAmount(ACCT_NUMBER3, 5000.00));
    }

    @Test
    void validate_initial_amount_cd() {
        assertTrue(bank.validateInitialAmountCD(5000.00));
        assertFalse(bank.validateInitialAmountCD(500));
    }

    @Test
    void validate_deposit_amount() {
        bank.addAccount(checking);
        bank.addAccount(savings);
        bank.addAccount(cd);
        assertTrue(bank.validDepositAmount(ACCT_NUMBER1, 700.00));
        assertTrue(bank.validDepositAmount(ACCT_NUMBER2, 1500.00));
        assertFalse(bank.validDepositAmount(ACCT_NUMBER3, 50.00));
    }

    @Test
    void get_accounts() {
        bank.addAccount(checking);
        bank.addAccount(savings);
        bank.addAccount(cd);
        assertEquals(3, bank.getAccounts().size());
    }

    @Test
    void deposit() {
        bank.addAccount(checking);
        bank.deposit(ACCT_NUMBER1, 500.00);
        assertEquals(500.00, bank.getBalance(ACCT_NUMBER1));
    }


}
