package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateTransferTest {
    public static Bank bank;
    public static TransferValidate validateInput;

    public String transfer = "transfer";
    public String transfer_not_enough_arguments = "transfer 12345678";
    public String transfer_typo = "tfanser 12345678 98765432 100.00";
    public String transfer_valid = "transfer 12345678 12345679 100.00";
    public String transfer_invalid_id = "transfer 123 125 100.00";
    public String transfer_invalid_amount = "transfer 12345678 12345677 sdfgsdfg";
    public String no_transfer = "12345678 12345677 100.00";
    public String transfer_case_insensitive = "tRANSfer 12345678 12345679 100.00";

    public String transfer_cd = "transfer 12345678 12345677 100.00";

    public String transfer_invalid_withdraw_amount = "transfer 12345678 12345679 500.00";

    public String transfer_withdraw_twice_savings = "transfer 12345679 12345678 50.00";

    //can't test for invalid deposit amounts as withdraw amount is always
    //less than or equal to max deposit amount in every case.
    //however, here's the only possible boundary case
    public String transfer_boundary_case = "transfer 12345679 12345678 1000.00";

    @BeforeEach
    void setup() {
        bank = new Bank();
        Checking checking = new Checking("12345678", 5.0);
        bank.addAccount(checking);
        bank.deposit("12345678", 100.00);
        Savings saving = new Savings("12345679", 5.0);
        bank.addAccount(saving);
        bank.deposit("12345679", 100.00);
        Cd cd = new Cd("12345677", 5.0, 100.0);
        bank.addAccount(cd);
        validateInput = new TransferValidate(bank);
    }

    @Test
    void validate_transfer_valid() {
        assertTrue(validateInput.validate(transfer_valid));
    }

    @Test
    void validate_transfer() {
        assertFalse(validateInput.validate(transfer));
    }

    @Test
    void validate_transfer_not_enough_arguments() {
        assertFalse(validateInput.validate(transfer_not_enough_arguments));
    }

    @Test
    void validate_transfer_typo() {
        assertFalse(validateInput.validate(transfer_typo));
    }

    @Test
    void validate_transfer_invalid_id() {
        assertFalse(validateInput.validate(transfer_invalid_id));
    }

    @Test
    void validate_no_transfer() {
        assertFalse(validateInput.validate(no_transfer));
    }

    @Test
    void validate_transfer_invalid_amount() {
        assertFalse(validateInput.validate(transfer_invalid_amount));
    }

    @Test
    void validate_transfer_case_insensitive() {
        assertTrue(validateInput.validate(transfer_case_insensitive));
    }

    @Test
    void validate_transfer_cd() {
        assertFalse(validateInput.validate(transfer_cd));
    }

    @Test
    void validate_transfer_invalid_withdraw_amount() {
        assertFalse(validateInput.validate(transfer_invalid_withdraw_amount));
    }

    @Test
    void validate_transfer_withdraw_twice_savings() {
        bank.withdraw("12345679", 5.00);
        assertFalse(validateInput.validate(transfer_withdraw_twice_savings));
    }

    @Test
    void validate_transfer_boundary_case() {
        bank.deposit("12345679", 1000.00);
        assertTrue(validateInput.validate(transfer_boundary_case));
    }
}