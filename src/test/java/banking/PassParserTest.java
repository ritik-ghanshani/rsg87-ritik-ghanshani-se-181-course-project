package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PassParserTest {
    public static Bank bank;
    public static PassParser passParser;

    public String[] command_1_months = {"pass", "1"};
    public String[] command_2_months = {"pass", "2"};
    public String[] command_3_months = {"pass", "3"};

    @BeforeEach
    void setup() {
        bank = new Bank();
        bank.addAccount(new Checking("12345678", 1.0));
        bank.addAccount(new Savings("12345679", 1.0));
        passParser = new PassParser();
    }

    @Test
    void validate_one_month_with_high_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 1000.00));
        bank.deposit("12345678", 150.00);
        bank.deposit("12345679", 150.00);
        passParser.parse(command_1_months, bank);
        assertEquals(150.125, bank.getBalance("12345678"));
        assertEquals(150.125, bank.getBalance("12345679"));
        assertEquals(1003.3375023152971, bank.getBalance("12345677"));
    }

    @Test
    void validate_two_month_with_high_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 1000.00));
        bank.deposit("12345678", 150.00);
        bank.deposit("12345679", 150.00);
        passParser.parse(command_2_months, bank);
        assertEquals(150.25010416666666, bank.getBalance("12345678"));
        assertEquals(150.25010416666666, bank.getBalance("12345679"));
        assertEquals(1006.6861435522987, bank.getBalance("12345677"));
    }

    @Test
    void validate_three_month_with_high_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 1000.00));
        bank.deposit("12345678", 150.00);
        bank.deposit("12345679", 150.00);
        passParser.parse(command_3_months, bank);
        assertEquals(150.37531258680553, bank.getBalance("12345678"));
        assertEquals(150.37531258680553, bank.getBalance("12345679"));
        assertEquals(1010.045960887182, bank.getBalance("12345677"));
    }

    @Test
    void validate_one_month_with_low_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 99.00));
        bank.deposit("12345678", 99);
        bank.deposit("12345679", 99);
        passParser.parse(command_1_months, bank);
        assertEquals(74.06166666666667, bank.getBalance("12345678"));
        assertEquals(74.06166666666667, bank.getBalance("12345679"));
        assertEquals(99.33041272921439, bank.getBalance("12345677"));
    }

    @Test
    void validate_two_month_with_low_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 99.00));
        bank.deposit("12345678", 99);
        bank.deposit("12345679", 99);
        passParser.parse(command_2_months, bank);
        assertEquals(49.10255138888889, bank.getBalance("12345678"));
        assertEquals(49.10255138888889, bank.getBalance("12345679"));
        assertEquals(99.66192821167755, bank.getBalance("12345677"));
    }

    @Test
    void validate_three_month_with_low_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 99.00));
        bank.deposit("12345678", 99);
        bank.deposit("12345679", 99);
        passParser.parse(command_3_months, bank);
        assertEquals(24.12263684837963, bank.getBalance("12345678"));
        assertEquals(24.12263684837963, bank.getBalance("12345679"));
        assertEquals(99.994550127831, bank.getBalance("12345677"));
    }

    @Test
    void validate_three_month_with_zero_balance() {
        bank.addAccount(new Cd("12345677", 1.0, 25.00));
        bank.deposit("12345678", 25.00);
        bank.deposit("12345679", 25.00);
        passParser.parse(command_3_months, bank);
        assertFalse(bank.idExists("12345678"));
        assertFalse(bank.idExists("12345679"));
        assertFalse(bank.idExists("12345677"));
    }

}
