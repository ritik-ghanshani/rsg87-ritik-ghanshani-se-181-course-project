package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferParserTest {
    public static Bank bank;
    public static TransferParser transferParser;

    public String[] checking_to_savings = {"transfer", "12345678", "12345679", "100.00"};
    public String[] savings_to_checking = {"transfer", "12345679", "12345678", "100.00"};

    @BeforeEach
    void setup() {
        bank = new Bank();
        bank.addAccount(new Checking("12345678", 1.00));
        bank.deposit("12345678", 100.00);
        bank.addAccount(new Savings("12345679", 1.00));
        bank.deposit("12345679", 100.00);
        transferParser = new TransferParser();
    }

    @Test
    void validate_checking_to_savings() {
        transferParser.parse(checking_to_savings, bank);
        assertEquals(200.00, bank.getBalance("12345679"));
        assertEquals(0.00, bank.getBalance("12345678"));
    }

    @Test
    void validate_savings_to_checking() {
        transferParser.parse(savings_to_checking, bank);
        assertEquals(200.00, bank.getBalance("12345678"));
        assertEquals(0.00, bank.getBalance("12345679"));
    }
}
