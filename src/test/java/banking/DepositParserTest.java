package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositParserTest {
    public static Bank bank;
    public static DepositParser depositParser;

    public String[] Command = {"deposit", "12345678", "100"};
    public String[] CommandEncore = {"deposit", "12345678", "100"};

    @BeforeEach
    void setup() {
        bank = new Bank();
        bank.addAccount(new Checking("12345678", 1.0));
        depositParser = new DepositParser();
    }

    @Test
    void parse_command() {
        depositParser.parse(Command, bank);
        assertEquals(bank.getAccounts().get(Command[1]).getBalance(), Double.parseDouble(Command[2]));
    }


    @Test
    void parse_command_encore() {
        depositParser.parse(Command, bank);
        depositParser.parse(CommandEncore, bank);
        assertEquals(bank.getAccounts().get(CommandEncore[1]).getBalance(), Double.parseDouble(Command[2]) + Double.parseDouble(CommandEncore[2]));
    }

}
