package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidatePassTest {
    public String zero_months = "pass 0";
    public String negative_months = "pass 0";
    public String valid_months = "pass 15";
    public String too_many_months = "pass 61";
    public String pass = "pass";
    public String typo_in_pass = "pssass 35";
    public String invalid_months = "pass two";
    public String case_insensitive_pass = "pAss 14";
    public String min = "pass 1";
    public String max = "pass 60";
    PassValidator validateInput;
    Bank bank;

    @BeforeEach
    void setup() {
        bank = new Bank();
        validateInput = new PassValidator(bank);
    }

    @Test
    void validate_negative_months() {
        assertFalse(validateInput.validate(negative_months));
    }


    @Test
    void validate_zero_months() {
        assertFalse(validateInput.validate(zero_months));
    }


    @Test
    void validate_min() {
        assertTrue(validateInput.validate(min));
    }


    @Test
    void validate_max() {
        assertTrue(validateInput.validate(max));
    }

    @Test
    void validate_valid_months() {
        assertTrue(validateInput.validate(valid_months));
    }

    @Test
    void validate_too_many_months() {
        assertFalse(validateInput.validate(too_many_months));
    }

    @Test
    void validate_pass() {
        assertFalse(validateInput.validate(pass));
    }

    @Test
    void validate_typo_in_pass() {
        assertFalse(validateInput.validate(typo_in_pass));
    }

    @Test
    void validate_invalid_months() {
        assertFalse(validateInput.validate(invalid_months));
    }

    @Test
    void validate_case_insensitive_pass() {
        assertTrue(validateInput.validate(case_insensitive_pass));
    }


}
