package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CdTest {

    public static final String ACCT_NUMBER = "12345678";
    public static final Double APR = 5.3;
    public static final Double STARTING_BALANCE = 100.00;
    public static Account cd;

    @BeforeEach
    void setup() {
        cd = new Cd(ACCT_NUMBER, APR, STARTING_BALANCE);
    }


    @Test
    void account_has_some_balance_initially() {
        assertEquals(100.00, cd.getBalance());
    }

    @Test
    void withdraw_money() {
        cd.withdraw(100.00);
        assertEquals(0, cd.getBalance());
    }

    @Test
    void overdraft_protection() {
        cd.withdraw(600.00);
        assertEquals(0.00, cd.getBalance());
    }

}
