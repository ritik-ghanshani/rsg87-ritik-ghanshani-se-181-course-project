package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WithdrawParseTest {
    Bank bank;
    WithdrawParser withdrawParser;

    String[] command_valid_withdraw_checking = {"withdraw", "12345678", "100.00"};
    String[] command_valid_withdraw_checking_more_than_balance = {"withdraw", "12345678", "1000.00"};
    String[] command_valid_withdraw_savings = {"withdraw", "12345679", "100.00"};
    String[] command_valid_withdraw_cd = {"withdraw", "12345677", "100.00"};

    @BeforeEach
    void setup() {
        bank = new Bank();
        bank.addAccount(new Checking("12345678", 1.0));
        bank.addAccount(new Savings("12345679", 1.0));
        bank.addAccount(new Cd("12345677", 1.0, 100.00));
        withdrawParser = new WithdrawParser();
    }

    @Test
    void validate_withdraw() {
        withdrawParser.parse(command_valid_withdraw_checking, bank);
        withdrawParser.parse(command_valid_withdraw_savings, bank);
        withdrawParser.parse(command_valid_withdraw_cd, bank);
        assertEquals(0, bank.getBalance("12345678"));
        assertEquals(0, bank.getBalance("12345677"));
        assertEquals(0, bank.getBalance("12345679"));
    }

    @Test
    void validate_withdraw_checking_more_than_balance() {
        withdrawParser.parse(command_valid_withdraw_checking_more_than_balance, bank);
        assertEquals(0, bank.getBalance("12345678"));
    }
}
