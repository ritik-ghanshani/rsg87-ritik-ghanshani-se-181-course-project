package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandStorageTest {
    public static CommandStorage commandStorage;
    Bank bank;

    @BeforeEach
    public void setup() {
        commandStorage = new CommandStorage();
        bank = new Bank();
    }


    @Test
    public void command_starts_with_an_empty_list() {
        assertEquals(0, commandStorage.getInvalidCommands().size());
    }

    @Test
    public void command_insert_invalid_once() {
        commandStorage.addInValidCommand("uh oh a bad command, what should I do?");
        assertEquals(commandStorage.getInvalidCommands().size(), 1);
    }

    @Test
    public void command_insert_invalid_twice() {
        commandStorage.addInValidCommand("uh oh a bad command, what should I do?");
        commandStorage.addInValidCommand("lol another bad command.");
        assertEquals(commandStorage.getInvalidCommands().size(), 2);
    }

    @Test
    public void command_insert_create() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        commandStorage.addValidCommand("create savings 12345679 0.01");
        commandStorage.addValidCommand("create cd 12345677 0.01 1000");
        bank.addAccount(new Checking("12345678", 0.01));
        bank.addAccount(new Savings("12345679", 0.01));
        bank.addAccount(new Cd("12345677", 0.01, 1000.00));
        assertEquals(3, commandStorage.getOutput(bank).size());
    }

    @Test
    public void command_insert_deposit() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        bank.addAccount(new Checking("12345678", 0.01));
        commandStorage.addValidCommand("deposit 12345678 500.00");
        commandStorage.addValidCommand("deposit 12345678 500.00");
        assertEquals(3, commandStorage.getOutput(bank).size());
    }

    @Test
    public void command_insert_withdraw() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        bank.addAccount(new Checking("12345678", 0.01));
        commandStorage.addValidCommand("deposit 12345678 500.00");
        commandStorage.addValidCommand("withdraw 12345678 400.00");
        assertEquals(3, commandStorage.getOutput(bank).size());
    }

    @Test
    public void command_insert_transfer() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        bank.addAccount(new Checking("12345678", 0.01));
        commandStorage.addValidCommand("create checking 12345679 0.01");
        bank.addAccount(new Checking("12345679", 0.01));
        commandStorage.addValidCommand("deposit 12345678 500.00");
        commandStorage.addValidCommand("transfer 12345678 12345679 400.00");
        assertEquals(5, commandStorage.getOutput(bank).size());
    }

    @Test
    public void command_insert_pass() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        bank.addAccount(new Checking("12345678", 0.01));
        commandStorage.addValidCommand("deposit 12345678 500.00");
        commandStorage.addValidCommand("pass 1");
        assertEquals(2, commandStorage.getOutput(bank).size());
    }

    @Test
    public void deleted_id_output() {
        commandStorage.addValidCommand("create checking 12345678 0.01");
        bank.addAccount(new Checking("12345678", 0.01));
        bank.deposit("12345678", 50.00);
        commandStorage.addValidCommand("deposit 12345678 50.00");
        bank.pass(4);
        commandStorage.addValidCommand("pass 3");
        assertEquals(0, commandStorage.getOutput(bank).size());
    }


}
