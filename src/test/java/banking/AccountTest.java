package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {

    public static final String ACCT_NUMBER1 = "12345678";
    public static final String ACCT_NUMBER2 = "12345679";
    public static final String ACCT_NUMBER3 = "12345677";
    public static final Double APR = 5.3;
    public static final Double STARTING_BALANCE = 500.00;
    public static Account checking;
    public static Account savings;
    public static Account cd;

    @BeforeEach
    void setup() {
        checking = new Checking(ACCT_NUMBER1, APR);
        savings = new Savings(ACCT_NUMBER2, APR);
        cd = new Cd(ACCT_NUMBER3, APR, STARTING_BALANCE);
    }

    @Test
    void account_has_no_balance_initially() {
        Account checking = new Checking(ACCT_NUMBER1, APR);
        Account savings = new Savings(ACCT_NUMBER2, APR);
        assertEquals(0.00, checking.getBalance());
        assertEquals(0.00, savings.getBalance());
    }

    @Test
    void deposit_nothing() {
        checking.deposit(0.00);
        savings.deposit(0.00);
        assertEquals(0.00, savings.getBalance());
        assertEquals(0.00, checking.getBalance());
    }

    @Test
    void deposit_money() {
        checking.deposit(500.00);
        savings.deposit(500.00);
        assertEquals(500.00, checking.getBalance());
        assertEquals(500.00, savings.getBalance());
    }

    @Test
    void deposit_money_twice() {
        checking.deposit(500.00);
        checking.deposit(600.00);
        savings.deposit(500.00);
        savings.deposit(600.00);
        assertEquals(1100.00, checking.getBalance());
        assertEquals(1100.00, savings.getBalance());
    }

    @Test
    void withdraw_money() {
        checking.deposit(7000.00);
        checking.withdraw(500.00);
        assertEquals(6500.00, checking.getBalance());
        savings.deposit(7000.00);
        savings.withdraw(500.00);
        assertEquals(6500.00, savings.getBalance());
    }

    @Test
    void withdraw_money_twice() {
        checking.deposit(7000.00);
        checking.withdraw(500.00);
        checking.withdraw(500.00);
        assertEquals(6000.00, checking.getBalance());
        savings.deposit(7000.00);
        savings.withdraw(500.00);
        savings.withdraw(500.00);
        assertEquals(6000.00, savings.getBalance());
    }

    @Test
    void overdraft_protection() {
        checking.deposit(500.00);
        checking.withdraw(600.00);
        assertEquals(0.00, checking.getBalance());
        savings.deposit(500.00);
        savings.withdraw(600.00);
        assertEquals(0.00, savings.getBalance());
    }

    @Test
    void max_deposit() {
        assertEquals(1000, checking.maxDeposit);
        assertEquals(2500, savings.maxDeposit);
        assertEquals(0, cd.maxDeposit);
    }

    @Test
    void can_transfer() {
        assertTrue(savings.canTransfer);
        assertTrue(checking.canTransfer);
        assertFalse(cd.canTransfer);
    }

    @Test
    void account_id() {
        assertEquals("12345678", checking.getAccountID());
        assertEquals("12345679", savings.getAccountID());
        assertEquals("12345677", cd.getAccountID());
    }

    @Test
    void apr() {
        assertEquals(APR, checking.getAPR());
        assertEquals(APR, savings.getAPR());
        assertEquals(APR, cd.getAPR());
    }

    @Test
    void validate_deposit_amount() {
        assertTrue(checking.validDepositAmount(700.00));
        assertTrue(savings.validDepositAmount(1500.00));
        assertFalse(cd.validDepositAmount(50.00));

    }

    @Test
    void validate_withdraw_amount() {
        assertTrue(checking.validWithdrawAmount(300.00));
        assertTrue(savings.validWithdrawAmount(500.00));
        assertFalse(cd.validWithdrawAmount(5000.00));
    }

    @Test
    void accrue() {
        checking.accrue();
        savings.accrue();
        cd.accrue();
        assertEquals(0, checking.getBalance());
        assertEquals(0, savings.getBalance());
        assertEquals(508.8920266682697, cd.getBalance());
    }

    @Test
    void pass() {
        checking.pass();
        savings.pass();
        cd.pass();
        assertEquals(0, checking.getBalance());
        assertEquals(0, savings.getBalance());
        assertEquals(508.8920266682697, cd.getBalance());
    }

    @Test
    void getAccountState() {
        assertEquals("Checking 12345678 0.00 5.30", checking.getAccountState());
        assertEquals("Savings 12345679 0.00 5.30", savings.getAccountState());
        assertEquals("Cd 12345677 500.00 5.30", cd.getAccountState());
    }
}
