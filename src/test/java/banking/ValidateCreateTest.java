package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateCreateTest {
    public static Bank bank;
    public static CreateValidate validateInput;
    public String createCommandChecking = "create checking 12345678 0.01";
    public String createCommandSavings = "create savings 12345678 0.01";
    public String createCommandCd = "create cd 12345678 0.01 1000";

    public String create = "create";
    public String create_type = "create cd";
    public String create_type_id = "create cd 12345678";
    public String no_create = "checking 12345678 0.01";
    public String create_extra_arguments = "create checking 12345678 0.6 new_account";
    public String no_amount = "create cd 12345678 0.01";
    public String invalid_type = "create spend 12345678 0.01";
    public String incorrect_order = "create 1234678 checking 0.01";
    public String id = "create checking 12345672 0.01";
    public String zero_id = "create checking 0 0.01";
    public String zeros_id = "create checking 00000000 0.01";
    public String long_id = "create checking 1234567890 0.01";
    public String short_id = "create checking 123456 0.01";
    public String zero_before_id = "create checking 012345678 0.01";

    public String create_typo = "crate checking 12345678 0.01";
    public String account_typo = "create chacking 12345678 0.01";
    public String case_insensitivity = "CREATE CHECKING 12345678 0.01";

    //TODO: tests assumming ID already exists
    public String id_already_exists = "create checking 876554321 0.01";

    public String zero_apr = "create checking 12345678 0";
    public String ten_apr = "create checking 12345678 10";
    public String boundary_plus_apr = "create checking 12345678 10.01";
    public String boundary_minus_apr = "create checking 12345678 9.99";
    public String extreme_positive_apr = "create checking 12345678 100";
    public String extreme_negative_apr = "create checking 1234678 -50";
    public String valid_apr_1 = "create checking 12345678 4";
    public String valid_apr_2 = "create checking 12345678 6";
    public String negative_apr = "create checking 12345678 -0.01";
    public String alphabetic_apr = "create checking 12345678 abd";
    public String greater_than_10_apr = "create checking 12345678 11";

    public String no_id = "create cd 0.01";
    public String no_type = "create 12345678 0.01";
    public String alphabetic_id = "create checking asbdfsd 0.01";

    public String alphabetic_amount = "create cd 12345678 0.01 asd";
    public String cd_with_no_amount = "create cd 12345678 0.01";
    public String cd_with_zero_amount = "create cd 12345678 0.01 0";
    public String cd_with_less_than_lower_boundary = "create cd 12345678 0.01 999.99";
    public String cd_with_equal_to_lower_boundary = "create cd 12345678 0.01 1000";
    public String cd_with_more_than_lower_boundary = "create cd 12345678 0.01 1000.01";
    public String cd_with_less_than_upper_boundary = "create cd 12345678 0.01 9999.99";
    public String cd_with_equal_to_upper_boundary = "create cd 12345678 0.01 10000";
    public String cd_with_more_than_upper_boundary = "create cd 12345678 0.01 10000.01";

    @BeforeEach
    void setup() {
        bank = new Bank();
        Checking checking = new Checking("87654321", 5.0);
        bank.addAccount(checking);
        validateInput = new CreateValidate(bank);
    }

    @Test
    void validate_create_checking() {
        assertTrue(validateInput.validate(createCommandChecking));
    }

    @Test
    void validate_create_savings() {
        assertTrue(validateInput.validate(createCommandSavings));
    }

    @Test
    void validate_create_cd() {
        assertTrue(validateInput.validate(createCommandCd));
    }

    @Test
    void validate_create() {
        assertFalse(validateInput.validate(create));
    }

    @Test
    void validate_create_type() {
        assertFalse(validateInput.validate(create_type));
    }

    @Test
    void validate_create_type_id() {
        assertFalse(validateInput.validate(create_type_id));
    }

    @Test
    void validate_no_create() {
        assertFalse(validateInput.validate(no_create));
    }

    @Test
    void validate_no_id() {
        assertFalse(validateInput.validate(no_id));
    }

    @Test
    void validate_no_type() {
        assertFalse(validateInput.validate(no_type));
    }

    @Test
    void validate_alphabetic_id() {
        assertFalse(validateInput.validate(alphabetic_id));
    }

    @Test
    void validate_alphabetic_apr() {
        assertFalse(validateInput.validate(alphabetic_apr));
    }

    @Test
    void validate_alphabetic_amount() {
        assertFalse(validateInput.validate(alphabetic_amount));
    }

    @Test
    void validate_long_id() {
        assertFalse(validateInput.validate(long_id));
    }

    @Test
    void validate_short_id() {
        assertFalse(validateInput.validate(short_id));
    }

    @Test
    void validate_negative_apr() {
        assertFalse(validateInput.validate(negative_apr));
    }

    @Test
    void validate_zero_apr() {
        assertTrue(validateInput.validate(zero_apr));
    }

    @Test
    void validate_greater_than_10_apr() {
        assertFalse(validateInput.validate(greater_than_10_apr));
    }

    @Test
    void validate_zeros_id() {
        assertTrue(validateInput.validate(zeros_id));
    }

    @Test
    void validate_create_extra_arguments() {
        assertFalse(validateInput.validate(create_extra_arguments));
    }

    @Test
    void validate_no_amount() {
        assertFalse(validateInput.validate(no_amount));
    }

    @Test
    void validate_invalid_type() {
        assertFalse(validateInput.validate(invalid_type));
    }

    @Test
    void validate_incorrect_order() {
        assertFalse(validateInput.validate(incorrect_order));
    }

    @Test
    void validate_id() {
        assertTrue(validateInput.validate(id));
    }

    @Test
    void validate_zero_id() {
        assertFalse(validateInput.validate(zero_id));
    }

    @Test
    void validate_zero_before_id() {
        assertFalse(validateInput.validate(zero_before_id));
    }

    @Test
    void validate_create_typo() {
        assertFalse(validateInput.validate(create_typo));
    }

    @Test
    void validate_account_typo() {
        assertFalse(validateInput.validate(account_typo));
    }

    @Test
    void validate_case_insensitivity() {
        assertTrue(validateInput.validate(case_insensitivity));
    }

    @Test
    void validate_ten_apr() {
        assertTrue(validateInput.validate(ten_apr));
    }

    @Test
    void validate_boundary_plus_apr() {
        assertFalse(validateInput.validate(boundary_plus_apr));
    }

    @Test
    void validate_boundary_minus_apr() {
        assertTrue(validateInput.validate(boundary_minus_apr));
    }

    @Test
    void validate_extreme_positive_apr() {
        assertFalse(validateInput.validate(extreme_positive_apr));
    }

    @Test
    void validate_extreme_negative_apr() {
        assertFalse(validateInput.validate(extreme_negative_apr));
    }

    @Test
    void validate_valid_apr_1() {
        assertTrue(validateInput.validate(valid_apr_1));
    }

    @Test
    void validate_valid_apr_2() {
        assertTrue(validateInput.validate(valid_apr_2));
    }

    @Test
    void validate_cd_with_no_amount() {
        assertFalse(validateInput.validate(cd_with_no_amount));
    }

    @Test
    void validate_cd_with_zero_amount() {
        assertFalse(validateInput.validate(cd_with_zero_amount));
    }

    @Test
    void validate_cd_with_less_than_lower_boundary() {
        assertFalse(validateInput.validate(cd_with_less_than_lower_boundary));
    }

    @Test
    void validate_cd_with_equal_to_lower_boundary() {
        assertTrue(validateInput.validate(cd_with_equal_to_lower_boundary));
    }

    @Test
    void validate_cd_with_more_than_lower_boundary() {
        assertTrue(validateInput.validate(cd_with_more_than_lower_boundary));
    }

    @Test
    void validate_cd_with_less_than_upper_boundary() {
        assertTrue(validateInput.validate(cd_with_less_than_upper_boundary));
    }

    @Test
    void validate_cd_with_equal_to_upper_boundary() {
        assertTrue(validateInput.validate(cd_with_equal_to_upper_boundary));
    }

    @Test
    void validate_cd_with_more_than_upper_boundary() {
        assertFalse(validateInput.validate(cd_with_more_than_upper_boundary));
    }

    @Test
    void validate_id_already_exists() {
        assertFalse(validateInput.validate(id_already_exists));
    }

}


