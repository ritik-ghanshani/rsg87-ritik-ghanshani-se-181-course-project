package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateParserTest {
    public static Bank bank;
    public static CreateParser createParser;

    public String[] checkingCommand = {"create", "checking ", "12345678 ", "0.01"};
    public String[] savingsCommand = {"create", "savings ", "98765432 ", "0.6"};
    public String[] cdCommand = {"create", "cd ", "12345678 ", "1.2", "2000"};


    @BeforeEach
    void setup() {
        bank = new Bank();
        createParser = new CreateParser();
    }

    @Test
    void parse_checking_command() {
        createParser.parse(checkingCommand, bank);
        assertTrue(bank.idExists(checkingCommand[2]) && (bank.getAccounts().get(checkingCommand[2]).getAPR() == Double.parseDouble(checkingCommand[3])));
    }

    @Test
    void parse_savings_command() {
        createParser.parse(savingsCommand, bank);
        assertTrue(bank.idExists(savingsCommand[2]) && (bank.getAccounts().get(savingsCommand[2]).getAPR() == Double.parseDouble(savingsCommand[3])));
    }

    @Test
    void parse_cd_command() {
        createParser.parse(cdCommand, bank);
        assertTrue(bank.idExists(cdCommand[2]) && (bank.getAccounts().get(cdCommand[2]).getAPR() == Double.parseDouble(cdCommand[3])));
    }

}
