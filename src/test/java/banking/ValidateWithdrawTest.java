package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateWithdrawTest {
    public static Bank bank;
    public static WithdrawValidate validateInput;
    public String withdraw_checking = "withdraw 12345678 300";
    public String withdraw_checking_max = "withdraw 12345678 400";
    public String withdraw_savings = "withdraw 12345679 400";
    public String withdraw_savings_max = "withdraw 12345679 1000";
    public String withdraw = "withdraw";
    public String withdraw_typo = "withddraw 12345678 400";
    public String account_does_not_exist = "withdraw 12344444 399";
    public String no_withdraw = "12345678 100";
    public String withdraw_extra_arguments = "withdraw checking 12345678 0.6 new_account";
    public String no_amount = "withdraw 12345678";
    public String incorrect_order = "withdraw 100 1234678";
    public String zero_id = "withdraw 0 100";
    public String zeros_id = "withdraw 00000000 100";
    public String long_id = "withdraw 1234567890 100";
    public String short_id = "withdraw 123456 100";
    public String zero_before_id = "withdraw 012345678 100";

    public String withdraw_cd_equal = "withdraw 12345677 10000";
    public String withdraw_cd_equal_after_accrual = "withdraw 12345677 12208.953550254198";
    public String withdraw_cd_zero = "withdraw 12345677 0";
    public String withdraw_cd_more = "withdraw 12345677 1000000";

    Checking checking;
    Savings saving;
    Cd cd;

    @BeforeEach
    void setup() {
        bank = new Bank();
        checking = new Checking("12345678", 5.0);
        bank.addAccount(checking);
        saving = new Savings("12345679", 5.0);
        bank.addAccount(saving);
        cd = new Cd("12345677", 5.0, 10000.0);
        bank.addAccount(cd);
        validateInput = new WithdrawValidate(bank);
    }

    @Test
    void validate_withdraw_checking() {
        assertTrue(validateInput.validate(withdraw_checking));
    }


    @Test
    void validate_withdraw_checking_max() {
        assertTrue(validateInput.validate(withdraw_checking_max));
    }

    @Test
    void validate_withdraw_savings() {
        assertTrue(validateInput.validate(withdraw_savings));
    }

    @Test
    void validate_withdraw_savings_max() {
        assertTrue(validateInput.validate(withdraw_savings_max));
    }

    @Test
    void validate_withdraw() {
        assertFalse(validateInput.validate(withdraw));
    }

    @Test
    void validate_withdraw_typo() {
        assertFalse(validateInput.validate(withdraw_typo));
    }

    @Test
    void validate_account_does_not_exist() {
        assertFalse(validateInput.validate(account_does_not_exist));
    }

    @Test
    void validate_no_withdraw() {
        assertFalse(validateInput.validate(no_withdraw));
    }

    @Test
    void validate_withdraw_extra_arguments() {
        assertFalse(validateInput.validate(withdraw_extra_arguments));
    }

    @Test
    void validate_no_amount() {
        assertFalse(validateInput.validate(no_amount));
    }

    @Test
    void validate_incorrect_order() {
        assertFalse(validateInput.validate(incorrect_order));
    }

    @Test
    void validate_zero_id() {
        assertFalse(validateInput.validate(zero_id));
    }

    @Test
    void validate_zeros_id() {
        assertFalse(validateInput.validate(zeros_id));
    }

    @Test
    void validate_long_id() {
        assertFalse(validateInput.validate(long_id));
    }

    @Test
    void validate_short_id() {
        assertFalse(validateInput.validate(short_id));
    }

    @Test
    void validate_zero_before_id() {
        assertFalse(validateInput.validate(zero_before_id));
    }

    @Test
    void withdraw_twice_in_one_month() {
        bank.withdraw("12345679", 100.00);
        assertFalse(validateInput.validate(withdraw_savings));
    }

    @Test
    void withdraw_twice_in_separate_months() {
        bank.deposit("12345679", 1000.00);
        bank.withdraw("12345679", 100.00);
        bank.pass(1);
        assertTrue(validateInput.validate(withdraw_savings));
    }

    @Test
    void withdraw_thrice_in_separate_months() {
        bank.deposit("12345679", 1000.00);
        bank.withdraw("12345679", 100.00);
        bank.pass(1);
        bank.withdraw("12345679", 100.00);
        assertFalse(validateInput.validate(withdraw_savings));
    }

    @Test
    void withdraw_cd_zero() {
        assertFalse(validateInput.validate(withdraw_cd_zero));
    }

    @Test
    void withdraw_equal_to_account_balance() {
        bank.deposit("12345677", 100.00);
        assertFalse(validateInput.validate(withdraw_cd_equal));
    }

    @Test
    void withdraw_equal_to_account_balance_after_12_months() {
        //Asserting false since in the next 12 months balance gets accrued
        //and total balance is more than initial balance
        bank.pass(12);
        assertFalse(validateInput.validate(withdraw_cd_equal));
    }

    @Test
    void withdraw_cd_zero_after_12_months() {
        bank.pass(12);
        assertFalse(validateInput.validate(withdraw_cd_zero));
    }

    @Test
    void withdraw_more_than_account_balance() {
        assertFalse(validateInput.validate(withdraw_cd_more));
    }

    @Test
    void withdraw_more_than_account_balance_after_12_months() {
        bank.pass(12);
        assertTrue(validateInput.validate(withdraw_cd_more));
    }

    @Test
    void withdraw_equal_account_balance_after_12_months() {
        bank.pass(12);
        assertTrue(validateInput.validate(withdraw_cd_equal_after_accrual));
    }

    @Test
    void withdraw_more_than_account_balance_after_15_months() {
        bank.pass(15);
        assertTrue(validateInput.validate(withdraw_cd_more));
    }

    @Test
    void withdraw_more_than_account_balance_after_5_months() {
        bank.pass(5);
        assertFalse(validateInput.validate(withdraw_cd_more));
    }
}
