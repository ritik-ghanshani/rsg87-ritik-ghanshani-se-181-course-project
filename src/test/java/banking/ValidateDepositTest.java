package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateDepositTest {
    public static Bank bank;
    public static DepositValidate validateInput;

    public String deposit_only = "deposit";
    public String no_deposit = "12345678 500";
    public String no_id = "deposit 500";
    public String typo = "desopit 12345678 400";
    public String case_insensitivity = "DesPoSIT 12345678 500";
    public String deposit_without_amount = "deposit 12345678";
    public String deposit_alpha_amount = "deposit 12345678 qwerty";
    public String deposit_valid_checking = "deposit 12345678 1000";
    public String deposit_valid_saving = "deposit 12345679 2000";
    public String deposit_extra_argument = "deposit 12345678 1000 ipad";

    public String short_id = "deposit 12346 500";
    public String id_doesnt_exist = "deposit 99999999 300";
    public String long_id = "deposit 123456788888 500";
    public String negative_amount = "deposit 12345678 -500";
    public String zero_amount = "deposit 12345678 0";
    public String large_amount_checkings = "deposit 12345678 100000";
    public String large_amount_savings = "deposit 12345678 100000";
    public String max_checkings = "deposit 12345678 1000";
    public String max_savings = "deposit 12345679 2500";
    public String cd = "deposit 12345677 1000";

    @BeforeEach
    void setup() {
        bank = new Bank();
        Checking checking = new Checking("12345678", 5.0);
        bank.addAccount(checking);
        Savings saving = new Savings("12345679", 5.0);
        bank.addAccount(saving);
        Cd cd = new Cd("12345677", 5.0, 100.0);
        bank.addAccount(cd);
        validateInput = new DepositValidate(bank);
    }

    @Test
    void validate_deposit_only() {
        assertFalse(validateInput.validate(deposit_only));
    }

    @Test
    void validate_no_deposit() {
        assertFalse(validateInput.validate(no_deposit));
    }

    @Test
    void validate_no_id() {
        assertFalse(validateInput.validate(no_id));
    }

    @Test
    void validate_typo() {
        assertFalse(validateInput.validate(typo));
    }

    @Test
    void validate_case_insensitivity() {
        assertFalse(validateInput.validate(case_insensitivity));
    }

    @Test
    void validate_deposit_without_amount() {
        assertFalse(validateInput.validate(deposit_without_amount));
    }

    @Test
    void validate_deposit_alpha_amount() {
        assertFalse(validateInput.validate(deposit_alpha_amount));
    }

    @Test
    void validate_deposit_valid() {
        assertTrue(validateInput.validate(deposit_valid_checking));
        assertTrue(validateInput.validate(deposit_valid_saving));
    }

    @Test
    void validate_deposit_extra_argument() {
        assertFalse(validateInput.validate(deposit_extra_argument));
    }

    @Test
    void validate_short_id() {
        assertFalse(validateInput.validate(short_id));
    }

    @Test
    void validate_long_id() {
        assertFalse(validateInput.validate(long_id));
    }

    @Test
    void validate_id_doesnt_exist() {
        assertFalse(validateInput.validate(id_doesnt_exist));
    }

    @Test
    void validate_negative_amount() {
        assertFalse(validateInput.validate(negative_amount));
    }

    @Test
    void validate_zero_amount() {
        assertTrue(validateInput.validate(zero_amount));
    }

    @Test
    void validate_large_amount_checkings() {
        assertFalse(validateInput.validate(large_amount_checkings));
    }

    @Test
    void validate_large_amount_savings() {
        assertFalse(validateInput.validate(large_amount_savings));
    }

    @Test
    void validate_max_checkings() {
        assertTrue(validateInput.validate(max_checkings));
    }

    @Test
    void validate_max_savings() {
        assertTrue(validateInput.validate(max_savings));
    }

    @Test
    void validate_cd() {
        assertFalse(validateInput.validate(cd));
    }


}
