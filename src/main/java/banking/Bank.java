package banking;

import java.util.LinkedHashMap;
import java.util.Map;

public class Bank {

    private Map<String, Account> accounts;

    public Bank() {
        accounts = new LinkedHashMap<>();
    }

    public void addAccount(Account account) {
        accounts.put(account.getAccountID(), account);
    }

    public Map<String, Account> getAccounts() {
        return this.accounts;
    }

    public double getBalance(String acctId) {
        return this.getAccounts().get(acctId).getBalance();
    }

    public void deposit(String acctId, double amount) {
        accounts.get(acctId).deposit(amount);
    }

    public void withdraw(String acctId, double amount) {
        this.getAccounts().get(acctId).withdraw(amount);
    }

    public void pass(int months) {
        for (int i = 0; i < months; i++) {
            accounts.keySet().removeIf(acctId -> !accounts.get(acctId).pass());
        }
    }

    public Boolean idExists(String acctId) {
        return accounts.containsKey(acctId);
    }

    public boolean validDepositAmount(String acctId, double amt) {
        return accounts.get(acctId).validDepositAmount(amt);
    }

    public boolean validateInitialAmountCD(double amt) {
        return amt >= 1000 && amt <= 10000;
    }

    public boolean validateWithdrawAmount(String acctId, double amt) {
        return this.getAccounts().get(acctId).validWithdrawAmount(amt);
    }

    public boolean validateAPR(double a) {
        return a >= 0 && a <= 10;
    }

    public boolean canTransfer(String acctId) {
        return this.getAccounts().get(acctId).canTransfer();
    }

    public String getAccountState(String acctId) {
        return getAccounts().get(acctId).getAccountState();
    }

    public double getTransferBalance(String acctId, double amt) {
        return Math.min(amt, this.getBalance(acctId));
    }

    public void transfer(String from, String to, double amt) {
        double available_money = this.getTransferBalance(from, amt);
        this.withdraw(from, amt);
        this.deposit(to, available_money);
    }
}