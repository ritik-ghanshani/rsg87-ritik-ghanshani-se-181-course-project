package banking;

public class Savings extends Account {

    private boolean withdrawnThisMonth;

    Savings(String accountNumber, double apr) {
        super(accountNumber, apr);
        this.maxDeposit = 2500.00;
        this.maxWithdraw = 1000.00;
        withdrawnThisMonth = false;
        this.canTransfer = true;
    }

    @Override
    public void withdraw(double amount) {
        super.withdraw(amount);
        withdrawnThisMonth = true;
    }

    @Override
    public boolean pass() {
        withdrawnThisMonth = false;
        return super.pass();
    }

    @Override
    public boolean validWithdrawAmount(double amt) {
        return super.validWithdrawAmount(amt) && !withdrawnThisMonth;
    }
}
