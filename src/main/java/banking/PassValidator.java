package banking;

public class PassValidator extends Validator {
    private Bank bank;

    PassValidator(Bank bank) {
        this.bank = bank;
    }

    private boolean validateSyntax(String[] command) {
        return command.length == 2 && command[0].equalsIgnoreCase("pass");
    }

    private boolean validateMonth(String months) {
        return months.matches("\\d+") && (1 <= Integer.parseInt(months) && Integer.parseInt(months) <= 60);
    }

    @Override
    public boolean validate(String cmd) {
        String[] command = spliceCommand(cmd);
        return validateSyntax(command) && validateMonth(command[1]);
    }
}
