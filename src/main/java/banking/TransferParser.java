package banking;

public class TransferParser extends ParserBase {
    @Override
    public void parse(String[] command, Bank bank) {
        bank.transfer(command[1], command[2], Double.parseDouble(command[3]));
    }
}
