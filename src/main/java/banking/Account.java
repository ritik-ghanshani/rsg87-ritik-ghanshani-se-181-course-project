package banking;

public abstract class Account {
    protected String accountNumber;
    protected double apr;
    protected double balance;
    protected double maxDeposit;
    protected double maxWithdraw;
    protected boolean canTransfer;

    Account(String accountNumber, double apr) {
        this.accountNumber = accountNumber;
        this.apr = apr;
        this.balance = 0.00;
        this.maxDeposit = 0.00;
        this.maxWithdraw = 0.00;
        this.canTransfer = false;
    }

    public String getAccountID() {
        return this.accountNumber;
    }

    public double getAPR() {
        return this.apr;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public boolean validDepositAmount(double amt) {
        return 0 <= amt && amt <= maxDeposit;
    }

    public boolean validWithdrawAmount(double amt) {
        return amt <= this.maxWithdraw;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
        if (this.balance <= 0) {
            this.balance = 0.00;
        }
    }

    protected void accrue() {
        double apr_conv = this.apr / 1200;
        this.deposit(this.balance * apr_conv);
    }

    public boolean pass() {
        if (this.balance <= 0) {
            return false;
        }
        if (this.balance < 100) {
            this.withdraw(25);
        }
        this.accrue();
        return true;
    }

    public boolean canTransfer() {
        return canTransfer;
    }

    public String getAccountState() {
        return this.getClass().getSimpleName() + " " + this.getAccountID() + " " + String.format("%.2f", this.getBalance()) + " " + String.format("%.2f", this.getAPR());
    }
}
