package banking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class CommandStorage {
    public LinkedHashMap<String, ArrayList<String>> validCommands;
    public ArrayList<String> invalidCommands;
    public HashMap<String, Integer[]> addSyntax;

    public CommandStorage() {
        validCommands = new LinkedHashMap<>();
        invalidCommands = new ArrayList<>();
        addSyntax = new HashMap<>();
        addSyntax.put("create", new Integer[]{2});
        addSyntax.put("deposit", new Integer[]{1});
        addSyntax.put("withdraw", new Integer[]{1});
        addSyntax.put("transfer", new Integer[]{1, 2});
        addSyntax.put("pass", new Integer[]{});
    }

    public void addValidCommand(String command) {
        String[] tokens = command.split(" ");

        Arrays.stream(addSyntax.get(tokens[0].toLowerCase())).forEach((integer) -> handleAdd(tokens[integer], command));

    }

    private void handleAdd(String acctId, String command) {
        String[] tokens = command.split(" ");
        if (validCommands.containsKey(acctId) && !tokens[0].equalsIgnoreCase("create")) {
            validCommands.get(acctId).add(command);
        } else if (validCommands.containsKey(acctId) && tokens[0].equalsIgnoreCase("create")) {
            validCommands.get(acctId).clear();
        } else {
            validCommands.put(acctId, new ArrayList<>());
        }
    }

    public void addInValidCommand(String command) {
        invalidCommands.add(command);
    }

    public ArrayList<String> getInvalidCommands() {
        return invalidCommands;
    }

    public ArrayList<String> getOutput(Bank bank) {
        ArrayList<String> toReturn = new ArrayList<>();
        this.validCommands.forEach((k, v) -> {
            if (bank.idExists(k)) {
                toReturn.add(bank.getAccountState(k));
                toReturn.addAll(v);
            }
        });
        toReturn.addAll(invalidCommands);
        return toReturn;
    }
}
