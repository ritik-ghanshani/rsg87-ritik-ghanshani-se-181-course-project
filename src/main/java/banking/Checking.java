package banking;

public class Checking extends Account {
    Checking(String accountNumber, double apr) {
        super(accountNumber, apr);
        this.maxDeposit = 1000.00;
        this.maxWithdraw = 400.00;
        this.canTransfer = true;
    }
}
