package banking;

public class DepositParser extends ParserBase {
    @Override
    public void parse(String[] command, Bank bank) {
        bank.deposit(command[1], Double.parseDouble(command[2]));
    }
}
