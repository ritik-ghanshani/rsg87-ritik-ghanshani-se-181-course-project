package banking;

public abstract class Validator {
    protected boolean validateId(String ID) {
        try {
            Integer.parseInt(ID);
        } catch (Exception e) {
            return false;
        }
        return ID.length() == 8;
    }

    protected String[] spliceCommand(String command) {
        return command.split(" ");
    }

    public abstract boolean validate(String cmd);

}
