package banking;

public class PassParser extends ParserBase {
    @Override
    public void parse(String[] command, Bank bank) {
        int month = Integer.parseInt(command[1]);
        bank.pass(month);
    }
}
