package banking;

public class DepositValidate extends Validator {
    private Bank bank;

    public DepositValidate(Bank bank) {
        this.bank = bank;
    }

    private boolean validateAmount(String amount, String id) {
        return amount.matches("\\d*\\.?\\d+") && bank.validDepositAmount(id, Double.parseDouble(amount));
    }

    private boolean validateSyntax(String[] command) {
        return command.length == 3 && command[0].equalsIgnoreCase("deposit");
    }

    @Override
    protected boolean validateId(String command) {
        return super.validateId(command) && bank.idExists(command);
    }

    @Override
    public boolean validate(String cmd) {
        String[] command = spliceCommand(cmd);
        return validateSyntax(command) && validateId(command[1]) && validateAmount(command[2], command[1]);
    }
}
