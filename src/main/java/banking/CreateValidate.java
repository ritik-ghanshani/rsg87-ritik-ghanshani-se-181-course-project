package banking;

import java.util.HashMap;

public class CreateValidate extends Validator {
    private Bank bank;
    private HashMap<String, Integer> accountTypes;

    CreateValidate(Bank bank) {
        this.bank = bank;
        accountTypes = new HashMap<>();
        accountTypes.put("cd", 5);
        accountTypes.put("checking", 4);
        accountTypes.put("savings", 4);
    }

    private boolean validateAPR(String apr) {
        return apr.matches("\\d*\\.?\\d+") && bank.validateAPR(Double.parseDouble(apr));
    }

    private boolean validateAmount(String amount) {
        return amount.matches("\\d*\\.?\\d+") && bank.validateInitialAmountCD(Double.parseDouble(amount));
    }

    private boolean validateSyntax(String[] command) {
        return command[0].equalsIgnoreCase("create") && command.length > 1
                && accountTypes.containsKey(command[1].toLowerCase())
                && command.length == accountTypes.get(command[1].toLowerCase());
    }

    @Override
    protected boolean validateId(String command) {
        return super.validateId(command) && !bank.idExists(command);
    }

    @Override
    public boolean validate(String cmd) {
        String[] command = spliceCommand(cmd);
        return validateSyntax(command) && validateAPR(command[3]) && (command.length != 5 || validateAmount(command[4])) && validateId(command[2]);
    }
}
