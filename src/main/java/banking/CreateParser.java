package banking;

public class CreateParser extends ParserBase {
    @Override
    public void parse(String[] command, Bank bank) {
        if (command.length == 5) {
            bank.addAccount(new Cd(command[2], Double.parseDouble(command[3]), Double.parseDouble(command[4])));
        } else if (command.length == 4) {
            if (command[1].equalsIgnoreCase("checking")) {
                bank.addAccount(new Checking(command[2], Double.parseDouble(command[3])));
            } else {
                bank.addAccount(new Savings(command[2], Double.parseDouble(command[3])));
            }
        }

    }
}
