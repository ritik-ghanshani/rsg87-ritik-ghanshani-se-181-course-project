package banking;

public class WithdrawValidate extends Validator {

    private Bank bank;


    WithdrawValidate(Bank bank) {
        this.bank = bank;
    }

    private boolean validateAmount(String acctId, String amount) {
        return amount.matches("\\d*\\.?\\d+") && bank.validateWithdrawAmount(acctId, Double.parseDouble(amount));
    }

    @Override
    protected boolean validateId(String command) {
        return super.validateId(command) && bank.idExists(command);
    }

    private boolean validateSyntax(String[] command) {
        return command[0].equalsIgnoreCase("withdraw") && command.length == 3;
    }

    @Override
    public boolean validate(String cmd) {
        String[] command = spliceCommand(cmd);
        return validateSyntax(command) && validateId(command[1]) && validateAmount(command[1], command[2]);
    }
}
