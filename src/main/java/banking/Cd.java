package banking;

public class Cd extends Account {
    protected int numberOfMonthsSinceCreation;

    Cd(String accountID, double APR, double balance) {
        super(accountID, APR);
        this.balance = balance;
        this.maxDeposit = 0.00;
        this.numberOfMonthsSinceCreation = 0;
        this.canTransfer = false;
    }

    @Override
    protected void accrue() {
        double apr_conv = this.apr / 1200;
        for (int i = 0; i < 4; i++) {
            this.balance += (this.balance * apr_conv);
        }

    }

    @Override
    public boolean validDepositAmount(double amt) {
        return false;
    }

    @Override
    public boolean pass() {
        this.numberOfMonthsSinceCreation += 1;
        return super.pass();
    }

    @Override
    public boolean validWithdrawAmount(double amt) {
        return amt >= this.balance && this.numberOfMonthsSinceCreation >= 12;
    }

    @Override
    public void withdraw(double amount) {
        if (amount >= this.balance) {
            this.balance = 0.00;
        }
    }

}
