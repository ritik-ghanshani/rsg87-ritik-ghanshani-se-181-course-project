package banking;

import java.util.List;

public class MasterControl {
    Bank bank;
    ValidateInput validateInput;
    Parser parser;
    CommandStorage commandStorage;

    public MasterControl(Bank bank, ValidateInput validateInput, Parser parser, CommandStorage commandStorage) {
        this.bank = bank;
        this.validateInput = validateInput;
        this.parser = parser;
        this.commandStorage = commandStorage;
    }

    public List<String> start(List<String> input) {
        for (String command : input) {
            if (validateInput.validate(command)) {
                parser.parse(command);
                commandStorage.addValidCommand(command);
            } else {
                commandStorage.addInValidCommand(command);
            }
        }
        return commandStorage.getOutput(bank);
    }
}
