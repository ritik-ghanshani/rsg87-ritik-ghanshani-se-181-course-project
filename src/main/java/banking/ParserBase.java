package banking;

public abstract class ParserBase {

    public abstract void parse(String[] command, Bank bank);
}
