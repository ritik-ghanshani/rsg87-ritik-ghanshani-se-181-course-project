package banking;

import java.util.ArrayList;

public class ValidateInput extends Validator {
    private ArrayList<Validator> validators;

    ValidateInput(Bank bank) {
        validators = new ArrayList<>();
        validators.add(new CreateValidate(bank));
        validators.add(new DepositValidate(bank));
        validators.add(new PassValidator(bank));
        validators.add(new TransferValidate(bank));
        validators.add(new WithdrawValidate(bank));
    }

    @Override
    public boolean validate(String command) {
        return this.validators.stream().map(v -> v.validate(command)).reduce(Boolean::logicalOr).orElse(false);
    }

}
