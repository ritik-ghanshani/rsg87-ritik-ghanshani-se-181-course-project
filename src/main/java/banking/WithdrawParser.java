package banking;

public class WithdrawParser extends ParserBase {

    @Override
    public void parse(String[] command, Bank bank) {
        bank.withdraw(command[1], Double.parseDouble(command[2]));
    }
}
