package banking;

public class TransferValidate extends Validator {
    private final Bank bank;

    TransferValidate(Bank bank) {
        this.bank = bank;
    }

    private boolean validateAmount(String[] command) {
        return command[3].matches("\\d*\\.?\\d+") && bank.validateWithdrawAmount(command[1], Double.parseDouble(command[3]));
    }

    public boolean validateSyntax(String[] command) {
        return command[0].equalsIgnoreCase("transfer") && command.length == 4;
    }

    @Override
    protected boolean validateId(String ID) {
        return super.validateId(ID) && bank.canTransfer(ID);
    }

    @Override
    public boolean validate(String cmd) {
        String[] command = spliceCommand(cmd);
        return validateSyntax(command) && validateId(command[1]) && validateId(command[2]) &&
                validateAmount(command);
    }
}
