package banking;

import java.util.HashMap;

public class Parser {
    Bank bank;
    HashMap<String, ParserBase> parsers;

    public Parser(Bank bank) {
        this.bank = bank;
        this.parsers = new HashMap<>();
        this.parsers.put("create", new CreateParser());
        this.parsers.put("deposit", new DepositParser());
        this.parsers.put("withdraw", new WithdrawParser());
        this.parsers.put("transfer", new TransferParser());
        this.parsers.put("pass", new PassParser());
    }

    public void parse(String command) {
        String[] tokens = command.split(" ");
        this.parsers.get(tokens[0].toLowerCase()).parse(tokens, bank);
    }
}
